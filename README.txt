Projet Optimisation / recherche opérationnelle
Master 1 informatique, Université du Littoral Côte d'Opale.

Deterministic Finite Automata Inference

see: 
http://www-lisic.univ-littoral.fr/~verel/TEACHING/19-20/RO-M1app/index.html
http://www-lisic.univ-littoral.fr/~verel/TEACHING/19-20/RO-M1/index.html


*** To compile the demo:

mkdir build
cd build
cmake ../src/exe
make


*** To runthe demo:
./demo