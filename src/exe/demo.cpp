/*
  demo.cpp

 Author:
  Sebastien Verel,
  Univ. du Littoral Côte d'Opale, France.

 Date:
   2019/09/03 : Version 0

***
    Demo of candidate solution evaluation

*** To compile from automata-inference directory:

mkdir build
cd build
cmake ../src/exe
make


*** To run:
./demo

*/
#include <iostream>
#include <fstream>
#include <random>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>
#include <eval/HillClimber.h>
#include <eval/TabuSearch.h>

using namespace std;

int main(int argc, char **argv) {

    int seed = atoi(argv[1]);

    Sample sample4("../instances/dfa_32_0_0.1_test-sample.json");

    HillClimber hc(50, sample4, "../results/results4hc.dat", 4, 2, seed);
    hc.run();

    TabuSearch ts(50, sample4,  "../results/results4ts.dat", 4, 2, seed);
    ts.run();

    Sample sample8("../instances/dfa_8_0_0.1_test-sample.json");
    hc.setSample(sample8);
    hc.setFileName("../results/results8hc.dat");
    hc.setNStates(8);
    hc.run();

    ts.setSample(sample8);
    ts.setFileName("../results/results8ts.dat");
    ts.setNStates(8);
    ts.run();

    Sample sample16("../instances/dfa_16_0_0.1_test-sample.json");
    hc.setSample(sample16);
    hc.setFileName("../results/results16hc.dat");
    hc.setNStates(16);
    hc.run();

    ts.setSample(sample16);
    ts.setFileName("../results/results16ts.dat");
    ts.setNStates(16);
    ts.run();

    Sample sample32("../instances/dfa_32_0_0.1_test-sample.json");
    hc.setSample(sample32);
    hc.setFileName("../results/results32hc.dat");
    hc.setNStates(32);
    hc.run();

    ts.setSample(sample32);
    ts.setFileName("../results/results32ts.dat");
    ts.setNStates(32);
    ts.run();
}