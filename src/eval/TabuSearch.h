#ifndef _tabuSearch_h
#define _tabuSearch_h

#include <utility>
#include <vector>
#include <random>
#include <chrono>
#include <base/solution.h>
#include <base/sample.h>
#include <eval/eval.h>
#include <iostream>
#include <fstream>
#include <random>
#include <rapidjson/document.h>
#include <rapidjson/istreamwrapper.h>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/ostreamwrapper.h>
#include <array>

using namespace rapidjson;
using namespace std;

class TabuSearch {

private:
    int nbEval;
    Sample sample;
    const char *fileName;
    int nStates;
    int alphabetSize;
    int first;
    int middle;
    int last;
    int seed;

public:
    explicit TabuSearch(int nbEval, Sample sample, const char *fileName, int nStates, int alphabetSize, int seed) : nbEval(nbEval),
                                            sample(std::move(sample)),
                                            fileName(fileName),
                                            nStates(nStates),
                                            alphabetSize(alphabetSize),
                                            seed(seed){

    }

    void setNStates(int nStates) {
        TabuSearch::nStates = nStates;
    }

    void setFileName(const char *fileName) {
        TabuSearch::fileName = fileName;
    }

    void setSample(const Sample &sample) {
        TabuSearch::sample = sample;
    }

    void run() {
        ofstream file;
        file.open(fileName);

        std::mt19937 graine(this->seed);
        std::uniform_int_distribution<int> dist(0, nStates-1);

        int i = 0;
        Solution<double> xSol;

        int tabuSearch[] = {};

        std::vector<Solution<double>> xSolUsed;

        SmartEval eval(graine, sample);

        xSol = initSolution(graine, dist);

        eval(xSol);
        double finalFit = 0;

        while (i < this->nbEval) {
            double fit = xSol.fitness();
            for (int stateArray = 0; stateArray < xSol.function.size(); stateArray++) {
                for (int stateValue = 0;
                     stateValue < xSol.function[stateArray].size(); stateValue++) {
                    int oldNumber = xSol.function[stateArray][stateValue];

                    //if the solution has already been explored, to loop
                    if (tabuSearch[stateArray] == stateValue) { continue; }

                    const int new_number = dist(graine);

                    xSol.function[stateArray][stateValue] = new_number;

                    eval(xSol);

                    if (xSol.fitness() > finalFit) {
                        finalFit = xSol.fitness();
                        this->first = stateArray;
                        this->middle = stateValue;
                        this->last = new_number;
                    }

                    xSolUsed.push_back(xSol);

                    tabuSearch[stateArray] = stateValue;

                    xSol.function[stateArray][stateValue] = oldNumber;
                }
            }
            xSol.function[this->first][this->middle] = this->last;
            file << i << "\t" << finalFit << "\n";
            i++;
        }
        file << finalFit << "\n";
        cout << finalFit << std::endl;
        file.close();
    }

    Solution<double> initSolution(std::mt19937 graine, std::uniform_int_distribution<int> dist){
        Solution<double> solution(nStates ,alphabetSize);
        for(auto & stateArray : solution.function){
            for(int & stateValue : stateArray){
                stateValue = dist(graine);
            }
        }

        return solution;
    }
};
#endif